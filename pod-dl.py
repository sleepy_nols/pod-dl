# wrapper around yt-dlp to download podcasts from podchaser
# by: nols
# license: GPLv3

import os
import re
import logging
from yt_dlp import YoutubeDL

URL_FILE_PATH = "podcasts.txt"
LOG_FILE_PATH = "pod-dl.log"
DONE_LIST_FILE_PATH = "done.txt"
DOWNLOAD_PATH_PREFIX = "download"
LOG_LVL = logging.INFO
pods = {}
done_list = []


# initialize logger
def init_logger():
    logger = logging.getLogger(__name__)
    logging.basicConfig(
        handlers=[
            logging.FileHandler(LOG_FILE_PATH),
            logging.StreamHandler()
        ],
        level=LOG_LVL)
    return logger


# get podcast urls from url file
def get_urls(url_file_path):
    f = open(url_file_path)
    urls = f.readlines()
    f.close()
    return urls


#
def write_done_list(list):
    string_list = ""
    for item in list:
        string_list += f"{item}\n"
    try:
        f = open(DONE_LIST_FILE_PATH, 'w')
    except Exception:
        log.error(f"Cloud not open file: {DONE_LIST_FILE_PATH}")
    f.write(string_list)
    f.close()


if __name__ == "__main__":
    POD_URLS = get_urls(URL_FILE_PATH)
    log = init_logger()

    log.debug(f"{POD_URLS=}")

    # parse podcast names
    for url in POD_URLS:
        match = re.search(
            r"(https://www\.podchaser\.com/podcasts/)((.+)-(\d+))", url)
        pod_full_name = match.group(2)
        pod_name = match.group(3).replace('-', '_')
        pod_id = match.group(4)
        pods[pod_id] = {
            'name': pod_name,
            'url': url,
        }
    log.debug(f"{pods=}")

    # create download prefix directory
    try:
        os.mkdir(DOWNLOAD_PATH_PREFIX)
    except FileExistsError:
        pass
    except FileNotFoundError:
        log.error(f"Path not found: {DOWNLOAD_PATH_PREFIX}")

    for pod in pods:
        # prepare folder
        pod_name = pods[pod]['name']
        pod_url = pods[pod]['url']
        download_path = f"{DOWNLOAD_PATH_PREFIX}/{pod_name}"

        # create pod dl folder
        try:
            os.mkdir(download_path)
            log.debug(f"Directory created: {download_path}")
        except FileExistsError:
            log.debug(f"Directory already exists: {download_path}")
        except FileNotFoundError:
            log.error(f"Path not found: {download_path}")

        # set download destination path
        ytdl_opts = {
            'paths': {
                'home': download_path,
            },
            # 'check_formats': True,
            'retries': 10,
            'file-access-retries': 10,
            'fragment-retries': 10,
        }
        # download
        try:
            log.info(f"Downloading: {pod_name}")
            with YoutubeDL(ytdl_opts) as ydl:
                error_code = ydl.download(pod_url)

            # add pod to list if finished without error
            done_list.append(pod_name)
            write_done_list(done_list)
            log.debug(f"{done_list=}")

        # exit loop on keyboard interrupt
        except KeyboardInterrupt:
            break
        except Exception as error:
            log.error(error_code)
            if error == "ERROR: unable to download video data: HTTP Error 404: Not Found":
                log.error(f"HTTP Error 404: Not Found")
        # except:
        #     log.error(f"Could not download {pod_name}")
